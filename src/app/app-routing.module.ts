import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ContactComponent} from "./contact/contact.component";
import {ContactDetailComponent} from "./contact-detail/contact-detail.component";
import {ContactEditComponent} from "./contact-edit/contact-edit.component";
import {ContactNewComponent} from "./contact-new/contact-new.component";
//import {ContactNewComponent} from "./contact-new/contact-new.component";

const routes: Routes = [
  { path: "", component: ContactComponent},
  { path: "contact/create", component: ContactNewComponent},
  { path: "contact/:nom", component: ContactDetailComponent},
  { path: "contact/edit/:nom", component: ContactEditComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
