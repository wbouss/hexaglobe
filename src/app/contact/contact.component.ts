import { Component, OnInit } from '@angular/core';
import {ContactService} from '../Service/contact.service';
import {IContact} from "./IContact";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})

export class ContactComponent implements OnInit {
  checkoutForm;
  list: Array<IContact>;
  listMatch: Array<IContact> = new Array();


  constructor( private formBuilder: FormBuilder,contactService: ContactService) {
    this.list = contactService.getContacts();
    this.listMatch = [...this.list];

    this.checkoutForm = formBuilder.group({
      search: '',
    });

  }

  ngOnInit(): void {
  }
  onSubmit(): void{
    if( typeof(this.checkoutForm.get('search').value) == 'undefined' || this.checkoutForm.get('search').value == '')
      this.listMatch = [...this.list];
    else {
      this.listMatch = [];
      for( let i = 0; i < this.list.length; i++){
        if(this.list[i].nom == this.checkoutForm.get('search').value)
        {
          this.listMatch.push(this.list[i]);
        }
      }
    }
  }



}
