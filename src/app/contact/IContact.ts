export interface IContact {
  nom: string,
  prenom: string,
  email: string,
  photo: string,
  tag: Array<string>
}
