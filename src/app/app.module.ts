import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactComponent } from './contact/contact.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NavigationContentComponent } from './navigation/navigation-content/navigation-content.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { ContactEditComponent } from './contact-edit/contact-edit.component';
import {ReactiveFormsModule} from "@angular/forms";
import { ContactNewComponent } from './contact-new/contact-new.component';

@NgModule({
  declarations: [
    AppComponent,
    ContactComponent,
    NavigationComponent,
    NavigationContentComponent,
    ContactDetailComponent,
    ContactEditComponent,
    ContactNewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
