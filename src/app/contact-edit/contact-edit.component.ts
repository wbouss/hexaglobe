import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {ContactService} from "../Service/contact.service";
import {IContact} from "../contact/IContact";

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css']
})
export class ContactEditComponent implements OnInit {

  checkoutForm;
  contact: IContact;
  name: string;

  constructor( route: ActivatedRoute, private  contactService: ContactService, form: FormBuilder) {
    this.contact = contactService.getContact(route.snapshot.params.nom);
    this.name = this.contact.nom;
    this.checkoutForm = form.group({
      nom: this.contact.nom,
      prenom: this.contact.prenom,
      email: this.contact.email,
      photo: this.contact.photo
    });
  }

  ngOnInit(): void {
  }

  onSubmit(contactToEdit) {
    this.contactService.editContact(this.name , contactToEdit);
  }

}
