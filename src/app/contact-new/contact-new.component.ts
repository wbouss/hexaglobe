import { Component, OnInit } from '@angular/core';
import {IContact} from "../contact/IContact";
import {ActivatedRoute} from "@angular/router";
import {ContactService} from "../Service/contact.service";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'app-contact-new',
  templateUrl: './contact-new.component.html',
  styleUrls: ['./contact-new.component.css']
})
export class ContactNewComponent implements OnInit {

  checkoutForm;
  contact: IContact;

  constructor( private  contactService: ContactService, form: FormBuilder) {
    this.checkoutForm = form.group({
      nom: '',
      prenom: '',
      email: '',
      photo: ''
    });
  }

  ngOnInit(): void {
  }

  onSubmit(contactToCreate) {
    this.contactService.createContact(contactToCreate);
  }
}
