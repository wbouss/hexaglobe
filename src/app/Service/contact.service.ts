import { Injectable } from '@angular/core';
import {IContact} from "../contact/IContact";
import * as contactsjson from '../contact/contacts.json';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  contacts: any = (contactsjson  as any).default;

  constructor() { }

  getContacts(): Array<IContact>{
    return this.contacts;
  }

  getContact(nom: string): IContact{
    var index = this.contacts.findIndex(obj => obj.nom === nom);
    return (index == -1) ? null : this.contacts[index];
  }

  editContact(nom: string, changes: IContact){
    var index = this.contacts.findIndex(obj => obj.nom === nom);
    this.contacts[index] = changes;
  }


  removeContact(nom: string){
    var index = this.contacts.findIndex(obj => obj.nom === nom);
    this.contacts.splice(index, 1);
    console.log(this.contacts);
  }

  createContact( newContact: IContact){
    this.contacts.push(newContact);
  }

}
