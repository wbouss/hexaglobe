import { Component, OnInit } from '@angular/core';
import {IContact} from "../contact/IContact";
import {ActivatedRoute, Router} from "@angular/router";
import {ContactService} from "../Service/contact.service";

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.css']
})
export class ContactDetailComponent implements OnInit {

  contact: IContact;

  constructor( private router: Router,  route: ActivatedRoute, private contactService: ContactService) {
    this.contact = contactService.getContact(route.snapshot.params.nom);
  }

  ngOnInit(): void {
  }

  destroy(): void {
    this.contactService.removeContact(this.contact.nom);
    this.router.navigate(['/']);
  }

}
